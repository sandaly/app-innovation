# -*- coding: utf-8 -*-

import xmltodict, json

file_path = "unlabeled_data/unlabeled_data.xml"
dico1 = {
			"index": {
				"_index":"test_debat",
				"_type":"tweet",
				"_id":""
				}
		}
dico2 = {
			"date": "",
			"favoris": "",
			"message": "",
			"retweet": "",
			"username": "",
			"hashtag": []
}

def load_unlabeled_tweets(file_path):
	with open(file_path) as fd:
		unlabeled_data = xmltodict.parse(fd.read())
	return unlabeled_data['root']['tweet']

def extract_hashtag(message):
	hashtags_by_tweet = []
	# items = []
		
	for  word in message.split():
		if word.startswith("#"):
			# items.append(word)
			hashtags_by_tweet.append(word)
	return hashtags_by_tweet



def convert_data_to_json():

	unlabeled_data = load_unlabeled_tweets(file_path)
	_id = 1
	for item in unlabeled_data:
		item = dict(item)
		dico1["index"].update({
				"_id": _id
				})

		dico2.update({
			"date":  item["date"],
			"favoris":  item["favoris"],
			"message": item["message"],
			"retweet": item["retweet"],
			"username": item["username"],
			"hashtag": extract_hashtag(item["message"])
			})

		with open('unlabeled_data/unlabeled_json_data.json', 'a') as fp:
		    json.dump(dico1, fp)
		    fp.write("\n")
		    json.dump(dico2, fp)
		    fp.write("\n")
		_id += 1

convert_data_to_json()