# -*- coding: utf-8 -*-
import codecs

d = {
	"emoticonesN":["😂","🤔","😒","😮","😩","😳","😭","😶","😕","😢","😉","😜","🖕🏽","😓","👎","😁"],
	"emoticonesP":["👏","👍"],
	"neutre":[ "#whatsapp", "#whatsappdown"],
	"negatif":["#Dégagez","#72","aah","...","...."],
	"motsPositifs":["Majestueux","Magistral","Magnifique","Merveilleux","Mignon","Agréable","Aimable","Talentueux","Félicitations","Succès"],
	"motsnegatifs":[],
	"lepen":["#LePen","#MLP","#MarineLePen","#lepen","#Marine2017","#Lepen","#Lepen!","#Marine","#MLP2017","lepen2017"],
	"lepenP":["#marinepresidente","#viveLepen","#JeVoteMarine"],
	"lepenN":["jevoteElleDegage","sortonslepen","7MaiContreLepen","#ToutSaufLePen"],
	"macron":["Macron","EmmanuelMacron","EM" ],
	"macronP":["#JeVoteMacron","#MacronPrésident"],
	"marconN":["#toutSaufMacron"]
	}

with open("negatifs.txt") as test:
	line = test.read()
	d["motsnegatifs"].append(line)
d[]


negatifs = []
positifs = []
neutres = []
mixtes = []

# reste = []


with open("unlabeled_messages_data.txt") as data:
	lines = data.read().split("\n")
	for line in lines:
		lineSplited = line.split()

		if len([line for emoticon in d["emoticonesN"] if emoticon in lineSplited]) > 0:
			negatifs.append(line+" negative")

		elif len([line for emoticon in d["emoticonesP"] if emoticon in lineSplited]) > 0:
			positifs.append(line+" positive")

		elif len([line for motNeutre in d["neutre"] if motNeutre in lineSplited]) > 0:
			neutres.append(line+" objective")

		elif len([line for hashtagNegatif in d["negatif"] if hashtagNegatif in lineSplited]) > 0:
			negatifs.append(line+" negative")

		elif len([line for motPositif in d["motsPositifs"] if motPositif in lineSplited]) > 0:
			positifs.append(line+" positive")
		
		elif len([line for motNegatif in d["motsnegatifs"] if motNegatif in lineSplited]) > 0:
			negatifs.append(line+" positive")

		elif len([line for hashtagMacron in d["macron"] if hashtagMacron in lineSplited]) > 0 and len([line for hashtagLepen in d["lepen"] if hashtagLepen in lineSplited]) > 0:
			negatifs.append(line+" negative")

		elif len([line for hashtagMacronP in d["macronP"] if hashtagMacronP in lineSplited]) > 0 and len([line for hashtagLepen in d["lepen"] if hashtagLepen in lineSplited]) > 0:
			mixtes.append(line+" mixed")
		
		elif (len([line for hashtagMacronP in d["macronP"] if hashtagMacronP in lineSplited]) > 0 or len([line for hashtagMacronP in d["macronP"] if hashtagMacronP in lineSplited]) > 0):
			positifs.append(line+" positive")
		else:
			reste.append(line)




# with open("reste.txt", "w") as f:
# 	for item in reste:
# 		f.write("%s\n" % item)
		





with open("indexed_messages_data.txt", "w") as f:
	for item in negatifs:
		f.write("%s\n" % item)

	for item in positifs:
			f.write("%s\n" % item)

	for item in neutres:
		f.write("%s\n" % item)
	
	for item in mixtes:
		f.write("%s\n" % item)


"""
neutre => neutre
emoticonesN => négatif
emoticonesP => positif
dictionnaire(negatif) => negative
dictionnaire(positif) => positif

Peut pas nécessaire à vérifier car le en considerant le dico positif c'est siffusant
-------------------------------------------------------------------------------------
lepenP ET dictionnaire(positive) => positive
macronP ET dictionnaire(positive) => positive 
(lepen ET macron) OU dictionnaire(negative)  => negative







macronP[] ET  lepen[]   = #mixte
macron ET  lepen  ET dictionnaire(negative) => #negative



( (macron OU macronP) et (non(lepen) et non(lepenP) OU  (dictionnaire(positif))    )  => positif (Pas nécessaire la conditions positif car la prémière avec le dictionnaire positif ça siffut)

lepen et non(macron) et non(maronP) et dictionnaire(positif)  => positif






"""




