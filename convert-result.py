import numpy as np
import re, os

labels= {"0":"autre",
        "1": " positif",
        "2":"negatif",
        "3": "mixte"}


sentences =[]
sentences_result = []

with open("data_deft2017/new-test-data.txt") as f:
    for line in f:
        sentences.append(line)

with open("resultat2.txt") as f:
    for line in f:
        sentences_result.append(line)

def convert_result():

    with open("resultat-test.txt", "w") as f:
        for x in xrange(len(sentences)):
            f.write(sentences[x].split()[0]+" "+ labels[sentences_result[x].split()[0]]+"\n")

convert_result()