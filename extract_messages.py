# -*- coding: utf-8 -*-

import xmltodict, json
import codecs

file_path = "unlabeled_data/unlabeled_data.xml"

dico2 = {
			"id_tweet": "",
			"message": "",
		}

def load_unlabeled_tweets(file_path):
	with codecs.open(file_path, 'r', "utf-8") as fd:
		unlabeled_data = xmltodict.parse(fd.read())
	return unlabeled_data['root']['tweet']

def extract_hashtag(message):
	hashtags_by_tweet = []
	# items = []
		
	for  word in message.split():
		if word.startswith("#"):
			# items.append(word)
			hashtags_by_tweet.append(word)
	return hashtags_by_tweet



def convert_data_to_json():

	unlabeled_data = load_unlabeled_tweets(file_path)
	
	with codecs.open('indexation/unlabeled_message_data.txt', 'a','utf-8') as text_file:
		for item in unlabeled_data:
			item = dict(item)

			text_file.write(item["url"].split("/")[-1] + " " + item["message"] + "\n")

convert_data_to_json()