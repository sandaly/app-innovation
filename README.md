# app-innovation

# Commandes pour inserer les données dans elasticsearch
 curl -X PUT "localhost:9200/test_debat" -H 'Content-Type: application/json' -d @mapping_debat
 curl -H 'Content-Type: application/x-ndjson' -XPOST 'localhost:9200/test_debat/_bulk?pretty' --data-binary @out.json

POUR SUPPRIMER UN INDEX:
 - curl -XDELETE "localhost:9200/test_debat/"  

# notes:

	- Alogo pour créer un sac de mot
		- http://www.insightsbot.com/blog/R8fu5/bag-of-words-algorithm-in-python-introduction

	- Créer un script pour Annoter les données visualisées depuis kibana.
	- Utiliser les données annotées plus les données d'apprentisage du tp pour l'apprentissage:
		- 1er cas:  en utilisant la méthode du tp (svm) et faire quelques ameliorations (Voir le tp pour faire les améliorations).
		- 2èm cas: utilisé Work2Vec pour la répresentation des doc en vecteur pour l'apprentissage du reseaux neurone.
		- Chercher dans quel cas on dois utliser CNN ( Convolutional neural networks )


# La commande pour créer le modèle d'apprentissage

liblinear-2.21/train -c 4 -e 0.1  data_train.svm  nom_fichier_model


liblinear-2.21/predict data_test.svm  nom_fichier_model fichier_resultat_apprentissage

La commande pour tester le modèle:

 curl -H 'Content-Type: application/x-ndjson' -XPOST 'localhost:9200/test_debat/_bulk?pretty' --data-binary @out.json

- Pour supprimer un index dans elasticsearch
curl -XDELETE 'http://localhost:9200/test_debat/'



Pour convertir le fichier xml en json à fin de pouvoir créer un fichier json de format qui respecte les formats de elastics search.

http://www.utilities-online.info/xmltojson/#.W-7lOOhKiUk


- Tuto install ElasticSearch
https://www.youtube.com/watch?v=16NeBf_IAmU

# Polarités
## Positifs:
	-	👏🏼,👍
## Négatif
	- Emoticone : 😂,🤔,😒,😮,😩,😳,😭,😶,😕,😢,😉,😜,🖕🏽,😓,👎
	- mots : honte
	- Les Hashtags:
		- honte,LePenMacron

		- JevoteMacron + MLP, Lepen, JeVoteelleDegage, 7maiContreLepen, MarineLepen
		-lepenMacron.
## Neutre 
	- Mots:
		- "je vote" AND honte










