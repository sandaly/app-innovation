import numpy as np
import re, os

labels= {"objective":0,
        "positive": 1,
        "negative": 2,
        "mixed": 3}

sentences =[]
sentences_test = []
sentences_train = []
listTEst = []
with open("data_deft2017/new-test-data.txt") as f:
    for line in f:
        sentences.append(line+" objective")
        sentences_test.append(line+ " objective")

with open("data_deft2017/task1-train.csv") as f:
    for line in f:
        sentences.append(line)
        sentences_train.append(line) 

def extract_words(sentence):
    words = re.sub(" ", " ",  sentence).split() #nltk.word_tokenize(sentence)
    words = words[0:(len(words) - 1)]
    words = words[1:(len(words))]
    # words_cleaned = [w.lower() for w in words]
    return words

def extract_label(sentence):
    words = re.sub(" ", " ",  sentence).split()
    label = words[ (len(words) - 1) : (len(words))]
    lable_cleaned = [w.lower() for w in label]
    return lable_cleaned


def tokenize_sentences(sentences):
    words = []
    for sentence in sentences:
        w = extract_words(sentence)
        words.extend(w)
    #Permet de supprimer les doublons afin de faire le BoW (Bag of Words)
    words = sorted(list(set(words)))
    return words

words = tokenize_sentences(sentences)

# for i,x in (enumerate(words,1)):
#     print i,x


    
def bagofwords(sentence, words):
    sentence_words = extract_words(sentence)
    label = extract_label(sentence)[0]

    # frequency word count

    bag = np.zeros(len(words) + 1)

    for sw in sentence_words:
        for i,word in (enumerate(words,1)):

            if word == sw: 
                bag[i] += 1
    #Pour initialiser le label de chaque phrase
    
    bag[0] += labels[label]
    return bag 

for item in sentences_train:
    listTEst.append(bagofwords(item,words))

#Ecrire dans le fichier svm.
f = open("data_train2.svm", "a")
for bag in listTEst:
    f.write(str(int(bag[0])) +" ") 
    for i in range(1,len(bag)):
        if(bag[i]!=0.0):
            f.write(str(i) + ":" + str(int(bag[i]))  +" ") 
    f.write("\n")




